// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TurretPawn.generated.h"

class UCameraComponent;
class USpringArmComponent;
class AAIDrone;
class AProjectileBase;

UCLASS()
class SPACESHOOTERRANGE_API ATurretPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATurretPawn();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		UStaticMeshComponent* BaseStaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		UStaticMeshComponent* BarrelStaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USceneComponent* ProjectileSpawnPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USpringArmComponent* SpringArmComponent;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rotation")
		float HRotationSpeed = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rotation")
		float VRotationSpeed = 100.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile Type")
	TSubclassOf<AProjectileBase> CurrentProjectile;

	void Fire();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<AActor>> NiagaraSystems;

	UFUNCTION(BlueprintImplementableEvent)
	void SpawnNiagaraSystem();

private:
	APlayerController* PlayerControllerRef;

	FQuat HorizontalRotationDirection;
	FQuat VerticalRotationDirection;

	void CalculateVerticalRotationInput(float Value);
	void CalculateHorizontalRotationInput(float Value);

	void TurnUp();
	void TurnRight();

	void SetupCameraRotation();

	// Shooting system
	void CheckFireCondition(AAIDrone* Target);

	FTimerHandle FireRateTimerHandle;

	float ReturnDistanceToDrone(AAIDrone* Target);

	// Seconds cooldown between shots
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting", meta = (AllowPrivateAccess = "true"))
	float FireRate = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting", meta = (AllowPrivateAccess = "true"))
	float FireRange = 500.0f;
};
