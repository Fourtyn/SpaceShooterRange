// Fill out your copyright notice in the Description page of Project Settings.


#include "TurretPawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "SpaceShooterRange/AI/AIDrone.h"
#include "ProjectileBase.h"
#include "ProjectileHeavy.h"
#include "Core/SSR_PlayerController.h"

// Debug: Logging
DEFINE_LOG_CATEGORY_STATIC(Log_TurretPawn, All, All)

// Sets default values
ATurretPawn::ATurretPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);
	// Adding Mesh
	BaseStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("BaseStaticMesh");
	BaseStaticMeshComponent->SetupAttachment(GetRootComponent());

	// SpringArm 
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArmComponent->SetupAttachment(GetRootComponent());

	// Adding Camera and binding it to the character with SpringArm
	CameraComponent = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraComponent->SetupAttachment(SpringArmComponent);

	// Setting the boolean variables in Camera Settings section of SpringArm
	// Camera rotation must be fixed vertically but rotate with the pawn horizontally
	SetupCameraRotation();


	// Add the barrel
	BarrelStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("BarrelStaticMesh");
	BarrelStaticMeshComponent->SetupAttachment(BaseStaticMeshComponent);

	ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
	ProjectileSpawnPoint->SetupAttachment(BarrelStaticMeshComponent);
}

// Called when the game starts or when spawned
void ATurretPawn::BeginPlay()
{
	Super::BeginPlay();
	
	//GetWorld()->GetTimerManager().SetTimer(FireRateTimerHandle, this, &ATurretPawn::Fire, FireRate, true, -1.0f);

	PlayerControllerRef = Cast<APlayerController>(GetController());
}

// Called every frame
void ATurretPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TurnUp();
	TurnRight();
}

// Called to bind functionality to input
void ATurretPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("TurnRight", this, &ATurretPawn::CalculateHorizontalRotationInput);
	PlayerInputComponent->BindAxis("TurnUp", this, &ATurretPawn::CalculateVerticalRotationInput);
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &ATurretPawn::Fire);

	//PlayerInputComponent->BindAction("StartShootingLaser", IE_Pressed, this, &ATurretPawn::StartShootingLaser);
	//PlayerInputComponent->BindAction("EndShootingLaser", IE_Released, this, &ATurretPawn::EndShootingLaser);
}

void ATurretPawn::CalculateVerticalRotationInput(float Value)
{
	float RotateAmount = Value * VRotationSpeed * GetWorld()->DeltaTimeSeconds;
	FRotator VRotation = FRotator(RotateAmount, 0, 0);
	VerticalRotationDirection = FQuat(VRotation);
}

void ATurretPawn::CalculateHorizontalRotationInput(float Value)
{
	float RotateAmount = Value * HRotationSpeed * GetWorld()->DeltaTimeSeconds;
	FRotator HRotation = FRotator(0, RotateAmount, 0);
	HorizontalRotationDirection = FQuat(HRotation);
}

void ATurretPawn::TurnUp()
{
	AddActorLocalRotation(VerticalRotationDirection, true);
}

void ATurretPawn::TurnRight()
{
	AddActorWorldRotation(HorizontalRotationDirection, true);
}

void ATurretPawn::SetupCameraRotation()
{
	SpringArmComponent->bUsePawnControlRotation = false;
	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritYaw = true;
	SpringArmComponent->bInheritRoll = false;
}

void ATurretPawn::CheckFireCondition(AAIDrone* Target)
{
	if (Target != nullptr /*&& Target Is NOT Dead*/ && ReturnDistanceToDrone(Target) <= FireRange)
	{
		UE_LOG(LogTemp, Warning, TEXT("FireCondition Success"));
		Fire();
	}
}

float ATurretPawn::ReturnDistanceToDrone(AAIDrone* Target)
{
	if (!Target)
	{
		// We need to return a value, even if we didn't process anything
		// TODO: set as float max value; or rearrange the function. Maybe somehow return nullptr
		return 0.0f;
	}
	return FVector::Dist(Target->GetActorLocation(), GetActorLocation());
}

void ATurretPawn::Fire()
{
	// Get ProjectileSpawnPointLocation && Rotation -> Spawn Projectile at Loc. towards Rot.

	if (CurrentProjectile->IsValidLowLevel())
	{
		// if(CurrentProjectileTypeAmount > 0) {...}
		FVector SpawnLocation = ProjectileSpawnPoint->GetComponentLocation();
		FRotator SpawnRotation = ProjectileSpawnPoint->GetComponentRotation();
		AProjectileBase* TempProjectile = GetWorld()->SpawnActor<AProjectileBase>(CurrentProjectile, SpawnLocation, SpawnRotation);
		TempProjectile->SetOwner(this);

		// Niagara system:
		ASSR_PlayerController* MyController = Cast<ASSR_PlayerController>(PlayerControllerRef);
		int CurrentNiagaraIndex = MyController->GetCurrentProjectileIndex() % NiagaraSystems.Num();
		if (NiagaraSystems[CurrentNiagaraIndex]->IsValidLowLevel())
		{
			AActor* TempNiagaraSpark = GetWorld()->SpawnActor<AActor>(NiagaraSystems[CurrentNiagaraIndex], SpawnLocation, SpawnRotation);
			TempNiagaraSpark->SetOwner(this);
			TempNiagaraSpark->InitialLifeSpan = 3.0f;
		}

		// Decrement the projectile counter
		if (TempProjectile->IsA(AProjectileHeavy::StaticClass()))
		{
			// Decrement CurrHeavyProjectileAmount
			// PlayerControllerRef->CurrHeavyProjectileAmount
		}
		else
		{
			// Decrement CurrBaseProjectileAmount
		}
	}
}