// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SSR_PlayerController.generated.h"

class ATurretPawn;
class AProjectileBase;

UCLASS()
class SPACESHOOTERRANGE_API ASSR_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

private:
	ATurretPawn* PlayerPawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<AProjectileBase>> Projectiles; // Placeholder for different Projectile types

	int CurrentProjectileIndex = 0;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	int DefaultBaseProjectileAmount = 10;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	int DefaultHeavyProjectileAmount = 5;

	void Shoot();
	void ChangeProjectile();
	void Recharge();

public:
	int CurrBaseProjectileAmount;
	int CurrHeavyProjectileAmount;

	int GetCurrentProjectileIndex();
};
