// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SSR_GameModeBase.generated.h"

class AAIDrone;
class ATurretPawn;
class AAISpawnPoint;
UCLASS()
class SPACESHOOTERRANGE_API ASSR_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASSR_GameModeBase();

	void ActorDied(AActor* DeadActor);

	bool CheckWaveDead();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Score")
	int TotalScore = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Score")
	int TargetsDown = 0;

	void AddScore(AAIDrone* DeadEnemy);

private:
	int TargetDronesCount = 0;

	// Use in future if the player would get killed
	ATurretPawn* PlayerTurret;

	void HandleGameStart();

	void HandleGameOver(bool bPlayerWon);

	int GetTargetsCount();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<AAIDrone>> DroneTypes; // Placeholder for different Drone types

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<AActor*> DroneSpawnPoints; // Placeholder for different Spawn Points of Drones

	void SpawnDrones();

	int CurrentWaveDronesAmount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<int> DronesInWavesAmount;

	int CurrentWaveIndex = 0;

	void UpdateWave();

protected:
	void BeginPlay() override;

	// In case of deriving other GameModes in future, those will be used to call Handle funcs from Blueprints
	UFUNCTION(BlueprintImplementableEvent)
	void GameStart();
	UFUNCTION(BlueprintImplementableEvent)
	void GameOver(bool bPlayerWon);

};
