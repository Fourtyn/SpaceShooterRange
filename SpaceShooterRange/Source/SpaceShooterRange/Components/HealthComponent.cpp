// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Core/SSR_GameModeBase.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = DefaultHealth;
	
	GameModeRef = Cast<ASSR_GameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));

	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);
}

void UHealthComponent::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("UHealthComponent::TakeDamage"))
	if (Damage != 0 && Health > 0)
	{
		// Limit health after damage. It must'nt be negative or above maximum
		Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);

		// If owner is dead, process ActorDied
		// <= 0 - in case of some change of programming code, such as if the Clapm would be removed. So that <=0 is considered a death state too
		if (Health <= 0)
		{
			if (GameModeRef)
			{
				GameModeRef->ActorDied(GetOwner());
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Health component has no reference to the GameMode"));
			}
		}
	}
}
