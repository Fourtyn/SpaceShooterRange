// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SSR_GameModeBase.h"
#include "SpaceShooterRange/Characters/TurretPawn.h"
#include "Core/SSR_PlayerController.h"
#include "AIDrone.h"
#include "Kismet/GameplayStatics.h"
#include "AISpawnPoint.h"

ASSR_GameModeBase::ASSR_GameModeBase()
{
	DefaultPawnClass = ATurretPawn::StaticClass();
	PlayerControllerClass = ASSR_PlayerController::StaticClass();
}

void ASSR_GameModeBase::UpdateWave()
{
	CurrentWaveIndex++;

	CurrentWaveDronesAmount = DronesInWavesAmount[CurrentWaveIndex];

	SpawnDrones();

	// Get Drones details
	TargetDronesCount = GetTargetsCount();
}

void ASSR_GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// Call HandleGameStart - init start countdown, turret activation, pawn check, etc.
	HandleGameStart();
}


void ASSR_GameModeBase::ActorDied(AActor* DeadActor)
{
	// Procces the death of the drone
	UE_LOG(LogTemp, Warning, TEXT("A Pawn Died"));

	// Check if DeadActor is a drone
	if (DeadActor->IsA(AAIDrone::StaticClass()))
	{
		// Destroy Drone and check if all drones in current wave are destroyed


		AAIDrone* DestroyedDrone = Cast<AAIDrone>(DeadActor);

		// Add score for killing the drone
		AddScore(DestroyedDrone);

		TargetsDown++;

		// Show message
		if (GEngine)
		{
			//GEngine->AddOnScreenDebugMessage(3, 3, FColor::Emerald, FString::Printf(TEXT("Enemy dead, +%d! Current score is: %d! TargetsDown: %d"), DestroyedDrone->ScoreValue, TotalScore, TargetsDown));
		}



		// Process VFX and destroy the actor
		DestroyedDrone->HandleDestruction();

		TargetDronesCount--;
		if (CheckWaveDead())
		{
			// TODO: Check the wave limit and spawn a new wave
			if (CurrentWaveIndex < DronesInWavesAmount.Num() - 1)
			{
				UpdateWave();
			}
			else
			{
				HandleGameOver(true);
			}
		}
	}
}

void ASSR_GameModeBase::HandleGameStart()
{
	// init start countdown, turret activation, pawn check, etc.
	// Get references and game win conditions

	// Get Turret details
	PlayerTurret = Cast<ATurretPawn>(UGameplayStatics::GetPlayerPawn(this, 0));

	// Call BP version GameStart()
	GameStart();

	// Spawn Drones
	CurrentWaveDronesAmount = DronesInWavesAmount[0];
	// Fill the array of Spawn Points
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAISpawnPoint::StaticClass(), DroneSpawnPoints);

	SpawnDrones();

	// Get Drones details
	TargetDronesCount = GetTargetsCount();
}

void ASSR_GameModeBase::HandleGameOver(bool bPlayerWon)
{
	// Check win conditions
	// Show results
	if (GEngine)
	{
		//GEngine->AddOnScreenDebugMessage(2, 120, FColor::Orange, FString::Printf(TEXT("Victory! Congratulations! The score is: %d!"), TotalScore));
	}

	// Call BP version GameOver()
	GameOver(bPlayerWon);
}

int ASSR_GameModeBase::GetTargetsCount()
{
	TArray<AActor*> TargetActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAIDrone::StaticClass(), TargetActors);

	return TargetActors.Num();
}

void ASSR_GameModeBase::SpawnDrones()
{
	if (DroneSpawnPoints.Num() < 0)
	{
		UE_LOG(LogTemp, Error, TEXT("Empty DroneSpawnPoints Array!"));
	}
	else
	{
		if (!DroneSpawnPoints.IsValidIndex(CurrentWaveDronesAmount - 1))
		{
			CurrentWaveDronesAmount = DroneSpawnPoints.Num();
		}

		for (int i = 0; i < CurrentWaveDronesAmount; i++)
		{
			GetWorld()->SpawnActor<AActor>(DroneTypes[rand() % DroneTypes.Num()], DroneSpawnPoints[i]->GetActorLocation(), DroneSpawnPoints[i]->GetActorRotation());
		}
	}
}

bool ASSR_GameModeBase::CheckWaveDead()
{
	if (TargetDronesCount == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ASSR_GameModeBase::AddScore(AAIDrone* DeadEnemy)
{
	TotalScore += DeadEnemy->ScoreValue;
}