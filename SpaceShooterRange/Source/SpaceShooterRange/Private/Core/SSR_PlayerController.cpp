// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SSR_PlayerController.h"
#include "Components/InputComponent.h"

// For working with player:
#include "TurretPawn.h"
#include "Kismet/GameplayStatics.h"

// For switching Projectile type - all Projectile class headers:
#include "ProjectileBase.h"
#include "ProjectileHeavy.h"

// Debug: Logging
DEFINE_LOG_CATEGORY_STATIC(Log_SSR_PlayerController, All, All)

void ASSR_PlayerController::BeginPlay()
{
	// Taking the first (and only, because singleplayer) player from the list
	PlayerPawn = Cast<ATurretPawn>(UGameplayStatics::GetPlayerPawn(this, 0));

	Recharge();
}

void ASSR_PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//InputComponent->BindAction("Shoot", IE_Pressed, this, &ATurretPawn::Fire);
	InputComponent->BindAction("ChangeProjectile", IE_Pressed, this, &ASSR_PlayerController::ChangeProjectile);
	InputComponent->BindAction("Recharge", IE_Pressed, this, &ASSR_PlayerController::ChangeProjectile);
}

void ASSR_PlayerController::Shoot()
{
	//PlayerPawn->Fire();
}

void ASSR_PlayerController::ChangeProjectile()
{
	if (Projectiles.Num() <= 1)
	{
		UE_LOG(LogTemp, Error, TEXT("ASSR_PlayerController::ChangeProjectile() - Projectiles.Num() <= 1"));
		return;
	}
	
	// Save previous index to compare with new index later
	int PreviousProjectileIndex = CurrentProjectileIndex;

	// Take the next index and loop through the array limit
	CurrentProjectileIndex = (CurrentProjectileIndex + 1) % Projectiles.Num();

	// New projectile is the same as the old one
	if (CurrentProjectileIndex == PreviousProjectileIndex)
	{
		UE_LOG(LogTemp, Error, TEXT("ASSR_PlayerController::ChangeProjectile() - CurrentProjectileIndex = PreviousProjectileIndex"));
		return;
	}

	TSubclassOf<AProjectileBase> CurrentProjectile = Projectiles[CurrentProjectileIndex];

	// if CurrProjectile == null => return;
	if(!CurrentProjectile->IsValidLowLevel())
	{
		UE_LOG(LogTemp, Error, TEXT("ASSR_PlayerController::ChangeProjectile() - !CurrentProjectile->IsValidLowLevel()"));
		return;
	}
	
	// Output of the chosen projectile type
	if (GEngine)
	{
		//GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Black, CurrentProjectile->GetName());
	}

	PlayerPawn->CurrentProjectile = CurrentProjectile;
}

void ASSR_PlayerController::Recharge()
{
	CurrBaseProjectileAmount = DefaultBaseProjectileAmount;
	CurrHeavyProjectileAmount = DefaultHeavyProjectileAmount;
}

int ASSR_PlayerController::GetCurrentProjectileIndex()
{
	return CurrentProjectileIndex;
}