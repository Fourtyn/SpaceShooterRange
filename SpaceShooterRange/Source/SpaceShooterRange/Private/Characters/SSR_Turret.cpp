// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/SSR_Turret.h"
#include "Components/StaticMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Debug: Logging
DEFINE_LOG_CATEGORY_STATIC(Log_SSR_TURRET_Pawn, All, All)

// Sets default values
ASSR_Turret::ASSR_Turret()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Root
	/*SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);*/

	// Adding Mesh
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	StaticMeshComponent->SetupAttachment(GetRootComponent());

	// SpringArm 
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
	SpringArmComponent->SetupAttachment(GetRootComponent());
	SpringArmComponent->bUsePawnControlRotation = true;

	// Adding Camera and binding it to the character with SpringArm
	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(SpringArmComponent);

	// Add the barrel
	BarrelStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("BarrelStaticMeshComponent");
	BarrelStaticMeshComponent->SetupAttachment(StaticMeshComponent);
}

// Called when the game starts or when spawned
void ASSR_Turret::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASSR_Turret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DoTurnRight();
	//DoTurnUp();
}

// Called to bind functionality to input
void ASSR_Turret::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("TurnRight", this, &ASSR_Turret::TurnRight);
	PlayerInputComponent->BindAxis("TurnUp", this, &ASSR_Turret::TurnUp);

	//PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &ASSR_Turret::Shoot);
	//PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &ASSR_Turret::Shoot);

}

void ASSR_Turret::TurnRight(float Value)
{
	double HorizontalRotationSpeed = 100.0f;
	HorizontalRotationSpeed *= Value;

	float RotateAmount = HorizontalRotationSpeed * GetWorld()->DeltaTimeSeconds;
	FRotator Rotation = FRotator(0, RotateAmount, 0);
	HorizontalRotationDirection = FQuat(Rotation);

	// TODO: �������� ����������-���� ����
	//UE_LOG(Log_SSR_TURRET_Pawn, Display, TEXT("Turn right: %f"), Value);

	/*FRotator CurrentRotation = SceneComponent->GetComponentRotation();
	CurrentRotation.Yaw += HorizontalRotationSpeed;
	SceneComponent->SetRelativeRotation(CurrentRotation);*/

	//AddControllerYawInput(Value);


	//FRotator currRotation = GetActorRotation();
	//SetActorRotation(currRotation);

	// Test of camera moving in location
	/*FVector currPos = this->GetActorLocation();
	currPos.X += HorizontalRotationSpeed * 3;
	this->SetActorLocation(currPos);*/

	/*FRotator CurrentRotation2 = this->GetActorRotation();
	CurrentRotation2.Yaw += HorizontalRotationSpeed;
	SetActorRotation(CurrentRotation2);*/

	//float CamRot = CameraComponent->GetComponentRotation().Yaw;
	UE_LOG(Log_SSR_TURRET_Pawn, Display, TEXT("Camera rotation yaw: %f"), CameraComponent->GetComponentRotation().Yaw);
	UE_LOG(Log_SSR_TURRET_Pawn, Display, TEXT("Actor rotation yaw: %f"), this->GetActorRotation().Yaw);
}

void ASSR_Turret::TurnUp(float Value)
{
	double VerticalRotationSpeed = 1.0f;
	VerticalRotationSpeed *= Value;

	float RotateAmount = VerticalRotationSpeed * GetWorld()->DeltaTimeSeconds;
	FRotator Rotation = FRotator(RotateAmount, 0, 0);
	VerticalRotationDirection = FQuat(Rotation);
	/*
	// TODO: �������� ����������-���� ����
	//UE_LOG(Log_SSR_TURRET_Pawn, Display, TEXT("Turn up: %f"), Value);
	FRotator currRotation = StaticMeshComponent->GetComponentRotation();
	//FRotator currRotation = GetActorRotation();
	currRotation.Pitch += VerticalRotationSpeed;
	//SetActorRotation(currRotation);
	StaticMeshComponent->SetRelativeRotation(currRotation);
	*/
}

void ASSR_Turret::Shoot()
{
	UE_LOG(Log_SSR_TURRET_Pawn, Display, TEXT("Shoot"));
}

void ASSR_Turret::DoTurnUp()
{
	AddActorLocalRotation(VerticalRotationDirection, true);
}

void ASSR_Turret::DoTurnRight()
{
	AddActorLocalRotation(HorizontalRotationDirection, true);
}

