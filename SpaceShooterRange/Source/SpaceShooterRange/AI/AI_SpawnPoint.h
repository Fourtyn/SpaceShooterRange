// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AI_SpawnPoint.generated.h"

UCLASS()
class SPACESHOOTERRANGE_API AAI_SpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAI_SpawnPoint();

	// For using random spawn points later and checking if the chosen Sp.Point is taken
	bool bTaken = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
