// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
//#include "SpaceShooterRange/Characters/TurretPawn.h"
#include "AIDrone.generated.h"

class UStaticMeshComponent;
class ATurretPawn;
class UHealthComponent;

UCLASS()
class SPACESHOOTERRANGE_API AAIDrone : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAIDrone();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		UStaticMeshComponent* BaseStaticMeshComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components")
		UHealthComponent* HealthComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Score")
		int ScoreValue = 100;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(VisibleAnywhere, Category = "AI")
	class UPawnSensingComponent* PawnSensingComp;

	UPROPERTY(EditAnywhere, Category = "AI")
	class UBehaviorTree* BehaviourTree;

	// Combat system
	void HandleDestruction();

private:

	// In case of adding drones firing at the player-turret in future, this function will be useful
	UFUNCTION()
	void OnPlayerCaught(APawn* Pawn);

	// To use to calculate start and end points location
	ATurretPawn* PlayerPawn;
};
