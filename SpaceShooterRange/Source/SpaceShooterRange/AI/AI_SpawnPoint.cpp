// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_SpawnPoint.h"

// Sets default values
AAI_SpawnPoint::AAI_SpawnPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AAI_SpawnPoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAI_SpawnPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

