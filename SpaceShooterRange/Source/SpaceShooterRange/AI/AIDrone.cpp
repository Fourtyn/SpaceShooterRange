// Fill out your copyright notice in the Description page of Project Settings.

//#include "AI.h"
#include "AIDrone.h"
//#include "AIDroneController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Perception/PawnSensingComponent.h"
#include "Kismet/GameplayStatics.h"
#include "SpaceShooterRange/Characters/TurretPawn.h"
#include "HealthComponent.h"

// Sets default values
AAIDrone::AAIDrone()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	// Adding Mesh
	BaseStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("BaseStaticMesh");
	BaseStaticMeshComponent->SetupAttachment(GetRootComponent());

	// Initialise Pawn senses
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComponent"));
	PawnSensingComp->SetPeripheralVisionAngle(90.f);

	// Initialise Health Component
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
}

// Called when the game starts or when spawned
void AAIDrone::BeginPlay()
{
	Super::BeginPlay();

	// Taking the first (and only, because singleplayer) player from the list
	PlayerPawn = Cast<ATurretPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
}

// Called every frame
void AAIDrone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAIDrone::OnPlayerCaught(APawn* Pawn)
{

}

void AAIDrone::HandleDestruction()
{
	// Play death effect, sound
	// Inform GameMode drone died - then destroy itself
	Destroy();
}
