// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileBase.h"
#include "ProjectileHeavy.generated.h"

/**
 *
 */
UCLASS()
class SPACESHOOTERRANGE_API AProjectileHeavy : public AProjectileBase
{
	GENERATED_BODY()
};
