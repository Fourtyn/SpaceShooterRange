#include "ProjectileLaser.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectileLaser::AProjectileLaser()
{
	//ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));

	// Laser is a static laser mesh, it doesn't need to move, only to be rotated
	ProjectileMovement->InitialSpeed = 0.0f;
	ProjectileMovement->MaxSpeed = 0.0f;

	// Laser will apply damage over time on collission - thus damage must be divided
	Damage = 0.5f;

	InitialLifeSpan = 0.0f;

	// The shooter is in space => no gravity by default;
	ProjectileMovement->ProjectileGravityScale = 0.0f;

	//Name = "�����";
}
//
//void AProjectileLaser::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
//	FVector NormalImpulse, const FHitResult& Hit)
//{
//	// Get TurretPawn pointer
//	AActor* MyOwner = GetOwner();
//	// If we couldn't get a valid reference, return
//	if (MyOwner)
//	{
//		// If collided actor is valid, collided actor != this projectile actor 
//		// AND if the collided actor is not the TurretPawn that spawned a Projectile
//		// Then apply damage
//		if (OtherActor && OtherActor != this && OtherActor != MyOwner)
//		{
//			UGameplayStatics::ApplyDamage(OtherActor, Damage, MyOwner->GetInstigatorController(), this, DamageType);
//			UE_LOG(LogTemp, Warning, TEXT("Projectile Applies Damage"));
//		}
//
//		// Here - play effects and sounds of projectile destruction
//
//		// Laser is not destroyed after the hit
//	}
//	else
//	{
//		return;
//	}
//}