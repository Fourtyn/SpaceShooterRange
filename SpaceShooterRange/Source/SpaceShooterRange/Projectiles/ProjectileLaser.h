#pragma once

#include "CoreMinimal.h"
#include "ProjectileBase.h"
#include "ProjectileLaser.generated.h"

/**
 * 
 */
UCLASS()
class SPACESHOOTERRANGE_API AProjectileLaser : public AProjectileBase
{
	GENERATED_BODY()

public:
	AProjectileLaser();

//protected:
//	UFUNCTION()
//	virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
//		FVector NormalImpulse, const FHitResult& Hit) override;
};
